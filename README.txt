
Encrypted fields user
-----------------

Module implements encoding user data encrypted by the module that enhances
the security of the personal data of users.

INSTALLATION:

Put the module in your Drupal modules directory and enable it in admin/modules.

Usage

Users are encoded importance to the role of "encrypted fields user".
To ensure that all users have encrypted fields you want to add this role
to all users.
Go to admin/config/people/user_settings/encrypted_fields
and select the checkboxes for the fields of user fields that you want to encode.

Options:

To provide access to "admin/config/people/user_settings/operations_fields"
authorize "admin/config/people/user_settings/operations_fields"
for the respective roles.
For coding fields already existing users, you need to turn on when the module
go to page "admin/config/people/user_settings/operations_fields"
and click "Start encoding" button.

Importantly! Before turning off the module you need
on the page "admin/config/people/user_settings/operations_fields"
click "Start decoding", and only then switch off the module.
