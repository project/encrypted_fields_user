<?php

/**
 * @file
 * Provides the view object type and associated methods.
 */

/**
 * The class implements the core functionality of the module.
 */
class EncryptedFieldsUserOperation {

  private $DefaultFields = array('name', 'mail');

  /**
   * Load variable_get().
   */
  public static function variableGet($name, $default = '') {
    return variable_get($name, $default);
  }

  /**
   * Save variable_set().
   */
  public static function variableSet($name, $value) {
    return variable_set($name, $value);
  }

  /**
   * Load field encrypted settings.
   */
  public function fieldsSettings() {
    $user_fields = array('name' => 'Name', 'mail' => 'Mail');
    $fields = $this->fieldsUserSql();
    if (!empty($fields)) {
      foreach ($fields as $value) {
        if (!empty($value->field_name)) {
          $label_array = explode('_', $value->field_name);
          unset($label_array[0]);
          $user_fields[$value->field_name] = ucfirst(implode(' ', $label_array));
        }
      }
    }
    return $user_fields;
  }

  /**
   * Load user field for settings form.
   */
  private function fieldsUserSql() {
    return db_select('field_config_instance', 'c')
      ->fields('c', array('field_name'))
      ->condition('c.entity_type', 'user')
      ->execute()
      ->fetchAll();
  }

  /**
   * Load users.
   */
  public function users($operation) {
    $users_array = '';
    $last_uid = $this->variableGet('encrypted_fields_user_uid_' . $operation, 0);
    $uid_encrypted = $this->encryptedFieldSql();
    if (isset($last_uid) && isset($uid_encrypted) && $last_uid > $uid_encrypted) {
      $last_uid = --$uid_encrypted;
    }
    $query = db_select('users', 'u');
    $query->innerJoin('users_roles', 'r', 'u.uid = r.uid');
    $query->fields('u', array('uid'));
    $query->condition('u.uid', $last_uid, '>');
    $roles = $this->variableGet('encrypted_fields_user_roles', '');
    if (!empty($roles)) {
      foreach (array_filter($roles, array('')) as $values) {
        if (!empty($values)) {
          $query->condition('r.rid', $values);
        }
      }
    }
    $users = $query->execute()->fetchAll();
    if (!empty($users)) {
      foreach ($users as $values) {
        if (!empty($values->uid)) {
          $users_array[] = $values->uid;
        }
      }
    }
    return $users_array;
  }

  /**
   * Load last user.
   */
  public function lastUsers($uid = 0, $max = FALSE) {
    $query = db_select('users', 'u');
    if (!empty($max)) {
      $query->addExpression('MAX(uid)');
    }
    else {
      $query->fields('u', array('uid'));
      $query->condition('u.uid', $uid, '>');
    }
    $user_id = $query->execute()->fetchField();
    return !empty($user_id) ? $user_id : '';
  }

  /**
   * Determining the status fields.
   */
  public function encriptedFields() {
    $status = $this->variableGet('encrypted_fields_user_status', '');
    $process = $this->variableGet('encrypted_fields_user_process', '');
    if (empty($process) && empty($status)) {
      return FALSE;
    }
    elseif (empty($status) || empty($process)) {
      $identuficator = !empty($status) ? $status : $process;
      if ($identuficator == 'encoding') {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Generate settings form.
   */
  public function generateForm($value) {
    $uid = $this->encryptedFieldSql();
    if ($value == 'decoding' && empty($uid)) {
      $value = 'encoding';
    }
    elseif ($value == 'encoding' && !empty($uid)) {
      $value = 'decoding';
    }
    $save = 'Start ' . $value;
    $form['operations'] = array(
      '#type' => 'hidden',
      '#value' => $value,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#submit' => array('_encrypted_fields_user_batch_form_submit'),
      '#value' => t($save),
    );
    return $form;
  }

  /**
   * Download users from using user_load_multiple().
   */
  public function usersLoad($uid) {
    return user_load_multiple($uid);
  }

  /**
   * Encoding and decoding, words with changing meanings.
   */
  public function operationsFields($user, $operation) {
    $fields = array_filter($this->variableGet('encrypted_fields_user_fields', ''));
    $edit_user = FALSE;
    foreach ($fields as $key => $fields_value) {
      $lang = field_language('user', $user, $key);
      if (!empty($fields_value) && !empty($user->$fields_value)) {
        if (in_array($key, $this->DefaultFields)) {
          $value_field = $user->$fields_value;
          $value = array(
            'language' => !empty($lang) ? $lang : '',
            'uid' => $user->uid,
            'field_name' => $key,
            'delta' => 0,
            'value' => $value_field,
          );
          $field_value = $this->Operation($operation, $value);
          $value['value'] = $field_value;
          $id = $this->encryptedFieldIsset(array('id' => $value_field));
          if ($operation == 'encrypt') {
            if (empty($id)) {
              $field_value = $this->encryptedFieldUserInsert($value);
            }
            else {
              $value['id'] = $id;
              $field_value = $this->encryptedFieldUserUpdate($value);
            }
          }
          else {
            if ($id) {
              $this->encryptedFieldUserDelete(array('id' => $value_field));
            }
            else {
              $this->encryptedFieldUserDelete(
                array('uid' => $user->uid),
                array('field_name' => $fields_value)
              );
            }
          }
          $user->$fields_value = $field_value;
        }
        else {
          if ($key == 'init') {
            $max_count = count($user->{$fields_value});
          }
          else {
            $max_count = count($user->{$fields_value}[$lang]);
          }
          for ($count = 0; $count < $max_count; $count++) {
            if (!empty($user->{$fields_value}[$lang][$count]) || $user->{$fields_value} == 'init') {
              $value_field = ($key == 'init') ? $user->{$fields_value} : $user->{$fields_value}[$lang][$count]['value'];
              $value = array(
                'language' => !empty($lang) ? $lang : '',
                'uid' => $user->uid,
                'field_name' => $key,
                'delta' => $count,
                'value' => $value_field,
              );
              $field_value = $this->operation($operation, $value);
              $value['value'] = $field_value;
              $id = $this->encryptedFieldIsset(array('id' => $value_field));
              if ($operation == 'encrypt') {
                if (empty($id)) {
                  $field_value = $this->encryptedFieldUserInsert($value);
                }
                else {
                  $value['id'] = $id;
                  $field_value = $this->encryptedFieldUserUpdate($value);
                }
              }
              else {
                if ($id) {
                  $this->encryptedFieldUserDelete(array('id' => $value_field));
                }
                else {
                  $this->encryptedFieldUserDelete(
                    array('uid' => $user->uid),
                    array('field_name' => $fields_value)
                  );
                }
              }
              if ($key == 'init') {
                $user->{$fields_value} = $field_value;
              }
              else {
                $user->{$fields_value}[$lang][$count]['value'] = $field_value;
              }
            }
          }
        }
        $edit_user = TRUE;
      }
    }
    if (!empty($edit_user)) {
      user_save($user);
    }
  }

  /**
   * The function of encryption and decryption user fields.
   */
  private function operation($operation, $values) {
    if ($operation == 'encrypt') {
      $result = encrypt($values['value'], array('base64' => TRUE));
    }
    elseif ($operation == 'decrypt_update') {
      $result = decrypt(utf8_decode($values['value']), array('base64' => TRUE));
    }
    else {
      $value = $this->encryptedFieldIsset(array('id' => $values['value']), array('value'));
      if (!empty($value)) {
        $result = decrypt(utf8_decode($value), array('base64' => TRUE));
      }
      else {
        $result = $values['value'];
      }
    }
    return $result;
  }

  /**
   * Saving a spreadsheet encrypted_fields_user new values.
   */
  private function encryptedFieldUserInsert($value) {
    $id = db_insert('encrypted_fields_user')
      ->fields(array(
        'language' => $value['language'],
        'uid' => $value['uid'],
        'field_name' => $value['field_name'],
        'delta' => $value['delta'],
        'value' => utf8_encode($value['value']),
      ))
      ->execute();
    return $id;
  }

  /**
   * Update table values of encrypted_fields_user.
   */
  private function encryptedFieldUserUpdate($value) {
    db_update('encrypted_fields_user')
      ->fields(array(
        'language' => $value['language'],
        'uid' => $value['uid'],
        'field_name' => $value['field_name'],
        'delta' => $value['delta'],
        'value' => utf8_encode($value['value']),
      ))
      ->condition('id', $value['id'])
      ->execute();
    return $value['id'];
  }

  /**
   * Deleting a table of values of encrypted_fields_user.
   */
  private function encryptedFieldUserDelete($value, $identuficator = array()) {
    $query = db_delete('encrypted_fields_user');
    foreach ($value as $id => $val) {
      if (isset($id) && isset($val)) {
        $query->condition($id, $val);
      }
    }
    if ($identuficator) {
      foreach ($identuficator as $id => $val) {
        if (isset($id) && isset($val)) {
          $query->condition($id, $val);
        }
      }
    }
    $result = $query->execute();
    return $result;
  }

  /**
   * Check for the existence of values in Table encrypted_fields_user.
   */
  public function encryptedFieldIsset($value, $fields = array('id')) {
    $query = db_select('encrypted_fields_user', 'e');
    $query->fields('e', $fields);
    foreach ($value as $id => $val) {
      if (isset($id) && isset($val)) {
        $query->condition($id, $val);
      }
    }
    $result = $query->execute()->fetchField();
    return $result;
  }

  /**
   * Check for encoding user permissions on roles.
   */
  public function encryptedRoleIsset($user) {
    $role_encrypted = user_role_load_by_name('encrypted fields user');
    if (!empty($role_encrypted) && array_key_exists($role_encrypted->rid, $user->roles)) {
      if (!empty($user->roles[$role_encrypted->rid])) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
    return FALSE;
  }

  /**
   * Decode user fields.
   */
  public function userFieldDecode($user) {
    $fields = array_filter($this->variableGet('encrypted_fields_user_fields', ''));
    foreach ($fields as $key => $fields_value) {
      $lang = field_language('user', $user, $key);
      if (!empty($fields_value) && !empty($user->$fields_value)) {
        if (in_array($key, $this->DefaultFields)) {
          $value_field = $user->$fields_value;
          $value = array(
            'language' => !empty($lang) ? $lang : '',
            'uid' => $user->uid,
            'field_name' => $key,
            'delta' => 0,
            'value' => $value_field,
          );
          $field_value = $this->operation('decrypt', $value);
          $user->$fields_value = $field_value;
        }
        else {
          if ($key == 'init') {
            $max_count = count($user->{$fields_value});
          }
          else {
            $max_count = count($user->{$fields_value}[$lang]);
          }
          for ($count = 0; $count < $max_count; $count++) {
            if (!empty($user->{$fields_value}[$lang][$count]) || $user->{$fields_value} == 'init') {
              $value_field = ($key == 'init') ? $user->{$fields_value} : $user->{$fields_value}[$lang][$count]['value'];
              $value = array(
                'language' => !empty($lang) ? $lang : '',
                'uid' => $user->uid,
                'field_name' => $key,
                'delta' => $count,
                'value' => $value_field,
              );
              $field_value = $this->operation('decrypt', $value);
              if ($key == 'init') {
                $user->{$fields_value} = $field_value;
              }
              else {
                $user->{$fields_value}[$lang][$count]['value'] = $field_value;
                $user->{$fields_value}[$lang][$count]['safe_value'] = $field_value;
              }
            }
          }
        }
      }
    }
    return $user;
  }

  /**
   * Update coded values user.
   */
  public function encryptedFieldUserFieldUpdate(&$user, $account) {
    $fields = array_filter($this->variableGet('encrypted_fields_user_fields', ''));
    if (empty($account->uid)) {
      $last_uid = $this->lastUsers(FALSE, TRUE);
      $account->uid = ++$last_uid;
    }
    foreach ($fields as $key => $fields_value) {
      $lang = field_language('user', $user, $key);
      if (!empty($fields_value) && !empty($user[$fields_value])) {
        if (in_array($key, $this->DefaultFields)) {
          $value_field = $user[$fields_value];
          if ($account->$fields_value) {
            $id = $this->encryptedFieldIsset(array('id' => $account->$fields_value));
          }
          $field_value = $this->operation('encrypt', array('value' => $value_field));
          $value = array(
            'language' => !empty($lang) ? $lang : '',
            'uid' => $account->uid,
            'field_name' => $key,
            'delta' => 0,
            'value' => $field_value,
          );
          if (empty($id)) {
            $field_value = $this->encryptedFieldUserInsert($value);
          }
          else {
            $value['id'] = $id;
            $field_value = $this->encryptedFieldUserUpdate($value);
          }
          $user[$fields_value] = $field_value;
        }
        else {
          if ($key == 'init') {
            $max_count = count($user->{$fields_value});
          }
          else {
            $max_count = count($user->{$fields_value}[$lang]);
          }
          for ($count = 0; $count < $max_count; $count++) {
            if (!empty($user->{$fields_value}[$lang][$count]) || $user->{$fields_value} == 'init') {
              $value_field = ($key == 'init') ? $user->{$fields_value} : $user->{$fields_value}[$lang][$count]['value'];
              if ($account->{$fields_value}[$lang][$count]['value'] || $account->{$fields_value} == 'init') {
                if ($account->{$fields_value} == 'init') {
                  $id = $this->encryptedFieldIsset(array('id' => $account->{$fields_value}));
                }
              else {
                  $id = $this->encryptedFieldIsset(array('id' => $account->{$fields_value}[$lang][$count]['value']));
                }
              }
              $field_value = $this->operation('encrypt', array('value' => $value_field));
              $value = array(
                'language' => !empty($lang) ? $lang : '',
                'uid' => $account->uid,
                'field_name' => $key,
                'delta' => 0,
                'value' => $field_value,
              );
              if (empty($id)) {
                $field_value = $this->encryptedFieldUserInsert($value);
              }
              else {
                $value['id'] = $id;
                $field_value = $this->encryptedFieldUserUpdate($value);
              }
              if ($key == 'init') {
                $user->{$fields_value} = $field_value;
              }
              else {
                $user->{$fields_value}[$lang][$count]['value'] = $field_value;
              }
            }
          }
        }
      }
    }
  }

  /**
   * Removing the values from the table when deleting own user.
   */
  public function encryptedFieldUserFieldDelete($uid) {
    $isseet_del = $this->encryptedFieldIsset(array('uid' => $uid));
    if (!empty($isseet_del)) {
      $this->encryptedFieldUserDelete(array('uid' => $uid));
    }
  }

  /**
   * Removing the values from the table when deleting user field.
   */
  public function encryptedFieldUserFieldProfileDelete($field) {
    $encrypted_fields = $this->variableGet('encrypted_fields_user_fields', array(''));
    if (!empty($encrypted_fields) && !empty($encrypted_fields[$field])) {
      unset($encrypted_fields[$field]);
      $this->variableSet('encrypted_fields_user_fields', $encrypted_fields);
    }
    $isseet_del = $this->encryptedFieldIsset(array('field_name' => $field));
    if (!empty($isseet_del)) {
      $this->encryptedFieldUserDelete(array('field_name' => $field));
    }
  }

  /**
   * The function that checks whether edited profile.
   */
  public function encryptedFieldUserEditProfile($user, $post) {
    foreach ($user as $value) {
      if ($value && array_intersect_key($post, (array) $value)) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Editing fields in the user authentication.
   */
  public function encryptedFieldUserAutorisation($field, $value) {
    $value_encode = $this->operation('encrypt', array('value' => $value));
    if (!empty($value_encode)) {
      $id = $this->encryptedFieldIsset(array('value' => utf8_encode($value_encode)));
    }
    return !empty($id) ? $id : '';
  }

  /**
   * Decoding single user field.
   */
  public function encryptedFieldDecode($value) {
    $values = array('value' => $value);
    $result = $this->operation('decrypt', $values);
    if (empty($result)) {
      $result = $value;
    }
    return $result;
  }

  /**
   * Editing fields for user token.
   */
  public function encryptedFieldTokenDecode($user, $field) {
    $fields = array_filter($this->variableGet('encrypted_fields_user_fields', ''));
    if (in_array($field, $fields)) {
      if (!empty($user->{$field})) {
        if (is_string($user->{$field})) {
          return $user->{$field};
        }
        else {
          $lang = field_language('user', $user, $field);
          if (!empty($user->{$field}[$lang][0]['value'])) {
            return $user->{$field}[$lang][0]['value'];
          }
        }
      }
    }
  }

  /**
   * Check whether blank table "encrypted_fields_user".
   */
  public function encryptedFieldSql() {
    return db_select('encrypted_fields_user', 'e')
      ->fields('e', array('uid'))
      ->execute()
      ->fetchField();
  }

}
